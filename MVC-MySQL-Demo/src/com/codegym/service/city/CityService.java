package com.codegym.service.city;

import com.codegym.model.City;


import java.util.List;


public interface CityService {
    List<City> findAll();

    void save(City city);

    City findById(String id);

    void update(String id, City city);

    void remove(String id,City city);
}