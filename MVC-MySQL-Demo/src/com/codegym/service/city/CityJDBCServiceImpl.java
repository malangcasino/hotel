package com.codegym.service.city;

import com.codegym.model.City;
import com.codegym.model.Customer;
import com.codegym.service.city.CityService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class CityJDBCServiceImpl implements CityService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/mvc_demo";
    private String jdbcUsername = "root";
    private String jdbcPassword = "Phuc@123z2";

    private static final String INSERT_USERS_SQL = "INSERT INTO City  (id, address,createBy,isDelete,createDate) VALUES "
            + " (?,?,?,?,?);";

    private static final String SELECT_USER_BY_ID = "select * from City where id =?";
    private static final String SELECT_ALL_USERS = "select * from City where  isDelete = 0";
    //private static final String DELETE_USERS_SQL = "delete from City where id = ?;";
    //Xoa Cung
    private static final String UPDATE_USERS_SQL = "update City set id = ?,address =?,modifiedBy = ?, modifiedDate = ? where id = ?;";
    //Xoa Mem
    private static final String DELETE_USERS_SQL = "update City set isDelete = 1 ,deleteBy = ?, deleteDate = ? where id = ?;";


    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


    @Override
    public List<City> findAll() {

        List<City> city = new ArrayList<>();
        try (Connection connection = getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);) {

            System.out.println(preparedStatement);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {

                String id = rs.getString("id");
                String address = rs.getString("address");
                Boolean isDelete = rs.getBoolean("isDelete");

                city.add(new City(id , address));

            }

        } catch (SQLException e) {
            printSQLException(e);
        }
        return city;
    }

    @Override
    public void save(City city) {
      //  System.out.println(INSERT_USERS_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {

            preparedStatement.setString(1, city.getId());
            preparedStatement.setString(2, city.getAddress());
            preparedStatement.setString(3, city.getCreateBy());
            preparedStatement.setInt(4,  city.isDelete());
            preparedStatement.setDate(5, Date.valueOf(city.createDate));
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
            // Insert,Update,Drop (Thuc hien trong mysql)
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public City findById(String id) {
        City city = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
            preparedStatement.setString(1,id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                id = rs.getString("id");
                String address = rs.getString("address");
                city = new City(id,address);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return city;
    }

    @Override
    public void update(String id, City city) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
            statement.setString(1,id);
            statement.setString(2, city.getAddress());
            statement.setString(3,city.getModifiedBy());
            statement.setString(5, city.getId());
            statement.setDate(4, Date.valueOf(city.getModifiedDate()));
            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
        //return rowUpdated;
    }

    @Override
    public void remove(String id,City city) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);)
        {
            statement.setString(1, city.getDeleteBy());
            statement.setDate(2, Date.valueOf(city.getModifiedDate()));
            statement.setString(3,city.getId());

        //    statement.setInt(1,city);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
