package com.codegym.service.customer;

import com.codegym.model.Customer;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;


public class CustomerJDBCServiceImpl implements CustomerService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/mvc_demo";
    private String jdbcUsername = "root";
    private String jdbcPassword = "Phuc@123z2";

    private static final String INSERT_USERS_SQL = "INSERT INTO Customer" + "  (name, email, address,isDelete,createBy,createDate) VALUES "
            + " (?, ?, ?,?,?,?);";
   private static final String SELECT_USER_BY_ID = "select id,name,email,address from Customer where id =?";
    private static final String SELECT_ALL_USERS = "select * from Customer where  isDelete = 0";
 //   private static final String DELETE_USERS_SQL = "delete from Customer where id = ?;";
    private static final String UPDATE_USERS_SQL = "update Customer set name = ?,email= ?, address =?,modifiedBy = ?,modifiedDate = ? where id = ?;";
    private static final String DELETE_USERS_SQL = "update Customer set isDelete = 1 ,deleteBy = ?, deleteDate = ? where id = ?;";


    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


    @Override
    public List<Customer> findAll() {
        // using try-with-resources to avoid closing resources (boiler plate code)
        List<Customer> users = new ArrayList<>();
        //B1.1: Tao mot Array list users kieu du lieu Customer
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             //B1.2: Thuc hien ket noi MySQL

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);) {
            //B1.3: Tao cau lenh bang cach su dung doi tuong ket noi SELECT_ALL_USERS
            //Truyen lenh sang sql
                System.out.println(preparedStatement);
                //in cau lenh ra
                // Step 3: Execute the query or update query
                ResultSet rs = preparedStatement.executeQuery();
                //Thuc hien truy van chon. No tra ve the hien cua mot danh sach ket qua ( bang)

                // Step 4: Process the ResultSet object.
                while (rs.next()) {
                    //Tra ve phan tu tiep theo tronng  danh sacsh va tien toi vi troi cont tro
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String email = rs.getString("email");
                    String address = rs.getString("address");

                    users.add(new Customer(id, name, email, address));

                }

        } catch (SQLException e) {
            printSQLException(e);
        }
        return users;
    }

    @Override
    public void save(Customer customer) {
        System.out.println(INSERT_USERS_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {

            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getEmail());
            preparedStatement.setString(3, customer.getAddress());
            preparedStatement.setInt(4,customer.isDelete());
            preparedStatement.setString(5,customer.getCreateBy());
            preparedStatement.setDate(6, Date.valueOf(customer.getCreateDate()));
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
            // Insert,Update,Drop (Thuc hien trong mysql)
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Customer findById(int id) {
        Customer customer = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String name = rs.getString("name");
                String email = rs.getString("email");
                String address = rs.getString("address");
                customer = new Customer(id, name, email, address);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return customer;
    }

    @Override
    public void update(int id, Customer customer) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getEmail());
            statement.setString(3, customer.getAddress());
            statement.setString(4,customer.getModifiedBy());
            statement.setDate(5, Date.valueOf(customer.getModifiedDate()));
            statement.setInt(6, customer.getId());

            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
        //return rowUpdated;
    }

    @Override
    public void remove(int id,Customer customer) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);)
        {

            statement.setString(1,customer.getDeleteBy());
            statement.setDate(2,Date.valueOf(customer.getDeleteDate()));
            statement.setInt(3, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
