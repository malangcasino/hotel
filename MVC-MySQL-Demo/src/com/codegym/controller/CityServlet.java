package com.codegym.controller;

import com.codegym.model.City;

import com.codegym.service.city.CityJDBCServiceImpl;
import com.codegym.service.city.CityService;
import com.codegym.service.customer.CustomerJDBCServiceImpl;
import com.codegym.service.customer.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.List;


@WebServlet(name = "CityServlet", urlPatterns = "/city")
public class CityServlet extends HttpServlet {


  private CustomerService customerService = new CustomerJDBCServiceImpl();
    private CityService cityService = new CityJDBCServiceImpl();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){

              case "delete":
                  deleteCustomercity(request, response);
                break;
              case "create":
                  createCity(request, response);
               break;
               case "edit":
                   updatecity(request, response);
               break;
            default:
                break;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){
            case "create":
                showCreateForm(request, response);
                break;
               case "list":
                listCities(request, response);
                break;
            case "edit":
                showEditFormCity(request, response);
                break;
            case "delete":
                showDeleteFormcity(request, response);
                break;
           default:
               listCities(request, response);
                break;
        }
    }


    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("city/create.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createCity(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        String address = request.getParameter("address");

        City city = new City(id, address);

        this.cityService.save(city);
        RequestDispatcher dispatcher = request.getRequestDispatcher("city/create.jsp");
        request.setAttribute("message", "New city was created");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void listCities(HttpServletRequest request, HttpServletResponse response) {
        List<City> cities = this.cityService.findAll();

        request.setAttribute("cities", cities);

        RequestDispatcher dispatcher = request.getRequestDispatcher("city/list.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void showEditFormCity(HttpServletRequest request, HttpServletResponse response) {

        String id = request.getParameter("id");
        City city = this.cityService.findById(id);
        RequestDispatcher dispatcher;
        if( city == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("city", city);
            dispatcher = request.getRequestDispatcher("city/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void updatecity(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");

        String address = request.getParameter("address");
        City city = this.cityService.findById(id);
        RequestDispatcher dispatcher;
        if(city == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            city.setId(id);
            city.setAddress(address);
            this.cityService.update(id, city);
            request.setAttribute("city", city);
            request.setAttribute("message", "City information was updated");
            dispatcher = request.getRequestDispatcher("city/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void showDeleteFormcity(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        City city = this.cityService.findById(id);
        RequestDispatcher dispatcher;
        if(city == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("city", city);
            dispatcher = request.getRequestDispatcher("city/delete.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void deleteCustomercity(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        City city = this.cityService.findById(id);
        RequestDispatcher dispatcher;
        if(city == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.cityService.remove(id,city);
            try {
                response.sendRedirect("/city");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
