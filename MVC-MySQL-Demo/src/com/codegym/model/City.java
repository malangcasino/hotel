package com.codegym.model;


import java.time.LocalDate;

import java.util.Date;

public class City {
    private String id;
    private String address;
    private int isDelete = 0;
    private String deleteBy;
    private LocalDate deleteDate = LocalDate.now();
    private String createBy;
    public LocalDate createDate = LocalDate.now();
    private String modifiedBy;
    private LocalDate modifiedDate = LocalDate.now();
    public City(){

    }
    public City(String id , String address) {
        this.id = id;
        this.address = address;
        this.createBy = "Phuc";
        this.modifiedBy = "Phuc";
        this.deleteBy = "Phuc";
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int isDelete() {
        return isDelete;
    }

    public String getDeleteBy() {
        return deleteBy;
    }

    public LocalDate getDeleteDate() {
        return deleteDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public  LocalDate getCreateDate() {
        return getCreateDate();
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public LocalDate getModifiedDate() {
        return modifiedDate;
    }
}
