package com.codegym.model;

import java.time.LocalDate;
import java.util.Date;

public class Customer {
    private int id;
    private String name;
    private String email;
    private String address;
    private int isDelete = 0;
    private String deleteBy = "Phuc";
    private LocalDate deleteDate = LocalDate.now();
    private String createBy = "Phuc";
    public LocalDate createDate = LocalDate.now();
    private String modifiedBy = "Phuc";
    private LocalDate modifiedDate = LocalDate.now();
    public Customer() {
    }

    public Customer(int id, String name, String email, String address) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int isDelete() {
        return isDelete;
    }

    public String getDeleteBy() {
        return deleteBy;
    }

    public LocalDate getDeleteDate() {
        return deleteDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public LocalDate getModifiedDate() {
        return modifiedDate;
    }
}
