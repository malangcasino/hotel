<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <meta charset="UTF-8">
    <title>Edit customer</title>
</head>
<body>
<h1>Edit customer</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/city?action=list">Back to city list</a>
</p>
<form method="post">
    <fieldset>
        <legend>City information</legend>
        <table>
            <tr>
                <td>Id: </td>
                <td><input type="text" name="id" id="id" value="${requestScope["city"].getId()}"></td>
            </tr
            ><tr>
                <td>Address: </td>
                <td><input type="text" name="address" id="address" value="${requestScope["city"].getAddress()}"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update city"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
