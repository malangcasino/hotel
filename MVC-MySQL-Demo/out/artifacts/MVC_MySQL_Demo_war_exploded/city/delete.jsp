<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Deleting customer</title>
</head>
<body>
<h1>Delete customer</h1>
<p>
    <a href="/city?action=list">Back to city list</a>
</p>
<form method="post">
    <h3>Are you sure?</h3>
    <fieldset>
        <legend>City information</legend>
        <table>

            <tr>
                <td>Address: </td>
                <td>${requestScope["city"].getAddress()}</td>
            </tr>
            <tr>
                <td><input type="submit" value="Delete City"></td>
                <td><a href="/city">Back to City list</a></td>
            </tr>
        </table>
    </fieldset>
</body>
</html>
