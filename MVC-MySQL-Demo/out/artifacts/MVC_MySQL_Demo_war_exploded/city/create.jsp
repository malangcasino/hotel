<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create new city</title>
    <style>
        .message{
            color:green;
        }
    </style>
</head>
<body>
    <h1>Create new city</h1>
    <p>
        <c:if test='${requestScope["message"] != null}'>
            <span class="message">${requestScope["message"]}</span>
        </c:if>
    </p>
    <p>
        <a href="/city">Back to city list</a>
    </p>
    <p>
        <a href="/customers?action=create">Create new customer</a>
    </p>
    <form name = "myForm"  onsubmit="return validateForm()" method="post">
        <fieldset>
            <legend>City information</legend>
            <table>
               <tr>
                    <td>Id: </td>
                    <td><input type="text" name="id" id="id"></td>
                </tr><tr>
                    <td>Address: </td>
                    <td><input type="text" name="address" id="address"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Create City"></td>
                </tr>
            </table>
        </fieldset>
    </form>
<script>
     // function  validateForm() {
     //     var x = document.forms["myForm"]["id"].value;
     //     var y = document.forms["myForm"]["address"].value;
     //     if(x==""||y=="") {
     //         alert("Please  input the value: ");
     //      return false;
     //     }
     // }

</script>
</body>
</html>
