<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 8:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View customer</title>
</head>
<body>
    <h1>Customer details</h1>
    <p>
        <a href="/city">Back to City list</a>
    </p>
    <table>
        <tr>
            <td>Name: </td>
            <td>${requestScope["city"].getName()}</td>
        </tr>
        <tr>
            <td>Email: </td>
            <td>${requestScope["city"].getEmail()}</td>
        </tr>
        <tr>
            <td>Address: </td>
            <td>${requestScope["city"].getAddress()}</td>
        </tr>
    </table>
</body>
</html>
