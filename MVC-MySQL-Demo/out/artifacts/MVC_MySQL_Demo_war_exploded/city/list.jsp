<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>City List</title>
</head>
<body>
    <h1>Cities</h1>
    <p>
        <a href="/city?action=create">Create new city</a>
    </p>
    <table border="1">

        </tr>
        <c:forEach items='${requestScope["cities"]}' var="cities">

<%--                <c:if test='${cities.isDelete() ==0 }'>--%>
                    <tr>
                        <td>${cities.getId()}</td>
                        <td>${cities.getAddress()}</td>
                        <td><a href="/city?action=edit&id=${cities.getId()}">edit</a></td>
                        <td><a href="/city?action=delete&id=${cities.getId()}">delete</a></td>
                    </tr>
<%--                </c:if>--%>


        </c:forEach>
    </table>
</body>
</html>
